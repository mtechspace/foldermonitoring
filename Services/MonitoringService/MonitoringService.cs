﻿using Microsoft.Extensions.FileSystemGlobbing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderMonitoring.Services.MonitoringService
{
    public class MonitoringService : IMonitoringService
    {
        String _monitorPath { get;  init; }
        private readonly FileSystemWatcher _watcher;
        private readonly ManualResetEvent _exitEvent;

        public MonitoringService(string monitorPath)
        {
            _monitorPath = monitorPath;
            _watcher = new FileSystemWatcher();
            _exitEvent = new ManualResetEvent(false);
        }

        public void StartMonitoring()
        {
            Console.WriteLine("Monitoring started");

            _watcher.Path = _monitorPath;
            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.FileName
                                 | NotifyFilters.DirectoryName;
            _watcher.Filter = "*.txt";
            _watcher.Created += OnChanged;
            _watcher.EnableRaisingEvents = true;

            _exitEvent.WaitOne();

            _watcher.Created -= OnChanged;
            _watcher.Dispose();
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");
        }

        public void StopMonitoring()
        {
            _exitEvent.Set();
        }
    }

}
