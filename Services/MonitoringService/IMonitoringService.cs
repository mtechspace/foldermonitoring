﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderMonitoring.Services.MonitoringService
{
    public interface IMonitoringService
    {
        public void StartMonitoring();
    }
}
