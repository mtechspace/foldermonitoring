﻿using FolderMonitoring.Services.MonitoringService;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace FolderMonitoring
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(BuildConfig)
                .ConfigureServices((context, services) =>
                {
                    var config = context.Configuration;
                    string relativeMonitorPath = config["FolderSettings:MonitorPath"] ?? "defaultPath";
                    string fullPath = Path.GetFullPath(relativeMonitorPath, AppDomain.CurrentDomain.BaseDirectory);

                    services.AddSingleton<IMonitoringService>(new MonitoringService(fullPath));
                })
                .Build();
            //Not sure how this app should be made, so took this approach, config would be a bit different if it was api
            //Since making this as console app- will use as is.
            //Also it would make sense to add logger like SeriLog but not doing that since no instructions about how it would be configured for prod were given
            //It would make sense since you could write logs also to a file pretty easily. 
            //For now, Console.Writeline() will work.

            var svc = host.Services.GetRequiredService<IMonitoringService>();
            svc.StartMonitoring();
        }

        static void BuildConfig(IConfigurationBuilder builder)
        {
            //Most likely if this app needed to go production different configuration would be used
            //One way that I have seen lately is deploying with docker compose and having "production" settings in there
            //Depends on the integration of overall service
            builder.SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                   .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

        }

    }
}
