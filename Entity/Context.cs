﻿using FolderMonitoring.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderMonitoring.Entity
{
    public class ContentContext : DbContext
    {
        public String DbPath { get; private set; }

        public ContentContext() : base()
        {
            DbPath = "Content.db";
        }

        public DbSet<Box> BoxEntities { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={DbPath}");
        }
    }
}
