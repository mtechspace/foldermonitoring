# Directory Monitoring Application

This repository contains a C# application designed to monitor changes in a specific directory. The application uses `FileSystemWatcher` to watch for file changes in a designated folder.

## Configuration

The application is configured through the `appsettings.json` file, which specifies the path to the directory that should be monitored.

### appsettings.json

In `appsettings.json`, the `MonitorPath` setting under `FolderSettings` determines the path to the directory being monitored. This path can be an absolute path or a relative path.

Example of `appsettings.json`:

```json
{
  "FolderSettings": {
    "MonitorPath": "./monitor"
  }
}
```

## Getting started

1) Clone the repository.
2) Ensure that .NET is installed on your machine.
3) Navigate to the solution directory and build the application.
4) Run the application. Make sure that the monitor folder exists or adjust the MonitorPath in appsettings.json as needed.
5) The application will start monitoring the specified directory for any file changes.

