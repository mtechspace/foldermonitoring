﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderMonitoring.Models
{
    public class Box
    {
        public string SupplierIdentifier { get; set; }
        public string Identifier { get; set; }

        public IReadOnlyCollection<Content> Contents { get; set; }

        public class Content
        {
            public string PoNumber { get; set; }
            public string Isbn { get; set; }
            public int Quantity { get; set; }
        }
    }
}
